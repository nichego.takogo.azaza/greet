package greet

func DoubleSum(a, b int) int {
	return double(a) + double(b)
}

func double(i int) int {
	return i + i
}
